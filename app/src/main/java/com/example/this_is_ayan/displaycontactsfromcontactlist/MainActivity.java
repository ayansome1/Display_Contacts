package com.example.this_is_ayan.displaycontactsfromcontactlist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends Activity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = (ListView) findViewById(R.id.listview);

                LoadContactsAyscn lc = new LoadContactsAyscn();
                lc.execute();
    }

    class LoadContactsAyscn extends AsyncTask<Void, Void, ArrayList<String>>
    {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute()
        {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pDialog = ProgressDialog.show(MainActivity.this, "Loading Contacts", "Please Wait");
        }

        @Override
        protected ArrayList<String> doInBackground(Void... params)
        {
            ArrayList<String> contacts = new ArrayList<String>();
            Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, null);
            while (c.moveToNext())
            {
                String contactName = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                contacts.add(contactName + "   :   " + phoneNumber);
            }
            c.close();
            return contacts;
        }

        @Override
        protected void onPostExecute(ArrayList<String> contacts)
        {
            super.onPostExecute(contacts);
            pDialog.cancel();
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.text, contacts);
            listView.setAdapter(adapter);
        }

    }
}
